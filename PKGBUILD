# Maintainer: Robin Broda <coderobe @ archlinux.org>
pkgname=nebula
pkgver=1.8.1
pkgrel=1
pkgdesc='A scalable overlay networking tool with a focus on performance, simplicity and security'
arch=(x86_64)
url='https://github.com/slackhq/nebula'
license=(MIT)
depends=(glibc)
makedepends=(go)
options=(!lto)
source=($pkgname-$pkgver.tar.gz::$url/archive/v$pkgver.tar.gz)
sha512sums=('c5c286adbdf287d9a0cc72149f35fabbaf86f076812ae8942888881c069e1b07620c85f24aecb8fc936c181d533972b20fa85275ae319a0527b4cebaa24e81e1')
b2sums=('9e08d324602105a82c965c158b99c753f4815fd314453efa013157a3365fe882baea9bdf4a5943a7a8eabd33a4dca0e14a2ee1437256d522080199fd596f2169')

prepare() {
  mkdir -vp $pkgname-$pkgver/build
}

build() {
  cd $pkgname-$pkgver

  export CGO_CPPFLAGS="$CPPFLAGS"
  export CGO_CFLAGS="$CFLAGS"
  export CGO_CXXFLAGS="$CXXFLAGS"
  export CGO_LDFLAGS="$LDFLAGS"
  export GOPATH="$srcdir"
  export GOFLAGS="-buildmode=pie -mod=readonly -modcacherw"

  go build -ldflags "-X main.Build=$pkgver -compressdwarf=false -linkmode external" -o build ./cmd/...
}

check() {
  cd $pkgname-$pkgver

  go test -v ./...
}

package() {
  cd $pkgname-$pkgver

  install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname/"
  install -Dm644 dist/arch/nebula.service -t "$pkgdir/usr/lib/systemd/system/"
  install -vDm 755 build/* -t "$pkgdir/usr/bin/"
}
